package ru.smochalkin.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;

public interface IBusinessService<E extends AbstractBusinessEntity> extends IService<E> {

    @NotNull
    E create(@NotNull String userId, @Nullable String name, @Nullable String description);

    void clear(String userId);

    @NotNull
    List<E> findAll(@NotNull String userId, @NotNull Comparator<E> comparator);

    @NotNull
    List<E> findAll(@NotNull String userId);

    @Nullable
    E findByName(@NotNull String userId, @Nullable String name);

    @NotNull
    E findByIndex(@NotNull String userId, @NotNull Integer index);

    void removeByName(@NotNull String userId, @Nullable String name);

    void removeByIndex(@NotNull String userId, @NotNull Integer index);

    void updateById(@Nullable String id, @Nullable String name, @Nullable String desc);

    void updateByIndex(@NotNull String userId, @NotNull Integer index, @Nullable String name, @Nullable String desc);

    void updateStatusById(@Nullable String id, @NotNull Status status);

    void updateStatusByName(@NotNull String userId, @Nullable String name, @NotNull Status status);

    void updateStatusByIndex(@NotNull String userId, @NotNull Integer index, @NotNull Status status);

    boolean isNotIndex(@NotNull String userId, @Nullable Integer index);

}
