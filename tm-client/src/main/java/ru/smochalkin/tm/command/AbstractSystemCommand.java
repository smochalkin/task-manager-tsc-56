package ru.smochalkin.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.smochalkin.tm.api.service.ICommandService;
import ru.smochalkin.tm.api.service.IPropertyService;

public abstract class AbstractSystemCommand extends AbstractCommand {

    @NotNull
    @Autowired
    protected ICommandService commandService;

    @Nullable
    public abstract String arg();

    @Override
    @NotNull
    public String toString() {
        @NotNull String result = super.toString();
        @Nullable final String arg = arg();
        if (arg != null && !arg.isEmpty()) result += " (" + arg + ")";
        return result;
    }

}
