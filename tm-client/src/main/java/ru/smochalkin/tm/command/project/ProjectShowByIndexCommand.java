package ru.smochalkin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.command.AbstractProjectCommand;
import ru.smochalkin.tm.endpoint.ProjectDto;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.exception.system.AccessDeniedException;
import ru.smochalkin.tm.util.TerminalUtil;

@Component
public final class ProjectShowByIndexCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String name() {
        return "project-show-by-index";
    }

    @Override
    @NotNull
    public String description() {
        return "Show project by index.";
    }

    @Override
    public void execute() {
        if (sessionService.getSession() == null) throw new AccessDeniedException();
        System.out.print("Enter index: ");
        @NotNull Integer index = TerminalUtil.nextInt();
        @NotNull final ProjectDto project = projectEndpoint.findProjectByIndex(sessionService.getSession(), --index);
        showProject(project);
    }

}
