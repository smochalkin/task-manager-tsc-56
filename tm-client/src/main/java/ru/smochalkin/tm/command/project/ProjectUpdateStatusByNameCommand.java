package ru.smochalkin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.command.AbstractProjectCommand;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.exception.system.AccessDeniedException;
import ru.smochalkin.tm.exception.system.StatusNotFoundException;
import ru.smochalkin.tm.util.TerminalUtil;

import java.util.Arrays;

@Component
public final class ProjectUpdateStatusByNameCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String name() {
        return "project-status-update-by-name";
    }

    @Override
    @NotNull
    public String description() {
        return "Update project status by name.";
    }

    @Override
    public void execute() {
        if (sessionService.getSession() == null) throw new AccessDeniedException();
        System.out.print("Enter name: ");
        String name = TerminalUtil.nextLine();
        System.out.println("Enter new status from list:");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusName = TerminalUtil.nextLine();
        projectEndpoint.changeProjectStatusByName(sessionService.getSession(), name, statusName);
    }

}
