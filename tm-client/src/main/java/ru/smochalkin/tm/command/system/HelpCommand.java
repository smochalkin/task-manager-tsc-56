package ru.smochalkin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.command.AbstractCommand;
import ru.smochalkin.tm.command.AbstractSystemCommand;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;

import java.util.Collection;

@Component
public final class HelpCommand extends AbstractSystemCommand {

    @Override
    @NotNull
    public String arg() {
        return "-h";
    }

    @Override
    @NotNull
    public String name() {
        return "help";
    }

    @Override
    @NotNull
    public String description() {
        return "Display list of terminal commands.";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        @NotNull final Collection<AbstractCommand> commands = commandService.getCommands();
        for (AbstractCommand command : commands) {
            System.out.println(command);
        }
    }

}
