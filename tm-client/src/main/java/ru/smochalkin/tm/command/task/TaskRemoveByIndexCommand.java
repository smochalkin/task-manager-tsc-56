package ru.smochalkin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.command.AbstractTaskCommand;
import ru.smochalkin.tm.endpoint.Result;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.exception.system.AccessDeniedException;
import ru.smochalkin.tm.util.TerminalUtil;

@Component
public final class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String name() {
        return "task-remove-by-index";
    }

    @Override
    @NotNull
    public String description() {
        return "Remove task by index.";
    }

    @Override
    public void execute() {
        if (sessionService.getSession() == null) throw new AccessDeniedException();
        System.out.print("Enter task index: ");
        @NotNull final Integer index = TerminalUtil.nextInt();
        @NotNull final Result result = taskEndpoint.removeTaskByIndex(sessionService.getSession(), index);
        printResult(result);
    }

}
