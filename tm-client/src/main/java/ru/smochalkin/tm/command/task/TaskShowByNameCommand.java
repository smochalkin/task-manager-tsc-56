package ru.smochalkin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.command.AbstractTaskCommand;
import ru.smochalkin.tm.endpoint.TaskDto;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.exception.system.AccessDeniedException;
import ru.smochalkin.tm.util.TerminalUtil;

@Component
public final class TaskShowByNameCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String name() {
        return "task-show-by-name";
    }

    @Override
    @NotNull
    public String description() {
        return "Show task by name.";
    }

    @Override
    public void execute() {
        if (sessionService.getSession() == null) throw new AccessDeniedException();
        System.out.print("Enter name: ");
        @NotNull final String name = TerminalUtil.nextLine();
        @Nullable final TaskDto task = taskEndpoint.findTaskByName(sessionService.getSession(), name);
        showTask(task);
    }

}
