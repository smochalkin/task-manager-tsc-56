package ru.smochalkin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.command.AbstractCommand;
import ru.smochalkin.tm.endpoint.Result;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.exception.system.AccessDeniedException;
import ru.smochalkin.tm.util.TerminalUtil;

@Component
public final class TaskUnbindByProjectIdCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "task-unbind-by-project-id";
    }

    @Override
    @NotNull
    public String description() {
        return "Unbind task to project by id.";
    }

    @Override
    public void execute() {
        if (sessionService.getSession() == null) throw new AccessDeniedException();
        System.out.print("Enter project id: ");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.print("Enter task id: ");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @NotNull final Result result = taskEndpoint.unbindTaskByProjectId(sessionService.getSession(), projectId, taskId);
        printResult(result);
    }

}
