package ru.smochalkin.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.dto.AbstractEntityDto;

import javax.validation.constraints.Null;
import java.util.List;

public interface IRepository<E extends AbstractEntityDto> {

    void clear();

    @NotNull
    List<E> findAll();

    @Nullable
    E findById(@Nullable String id);

    void removeById(@Nullable String id);

    int getCount();

}