package ru.smochalkin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.dto.LogDto;

public interface IJmsService {

    void send(@NotNull LogDto entity);

    LogDto createMessage(@NotNull Object object, @NotNull String type);

}
